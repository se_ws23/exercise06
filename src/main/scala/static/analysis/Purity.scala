package static.analysis

class A(val i: Int, var f: Float)

object Purity {
  var E: Double = Math.E
  val PI: Double = Math.PI
  val a: A = A(7, 0.04f)
  
  def f: Double = 1 / Math.pow(E, 7)
  
  def g(n: Int): Double =
    var res = 0.0
    var i = n.abs
    while(i > 0)
      res = res + 2 * PI * (a.i - 1)
      i = i - 1
    res
  
  def h(x: Float, y: Float): Float =
    if (Float.MinPositiveValue < a.f && a.f <= 1)
      1.2f * g(7).toFloat - x
    else
      2.9f * g(4).toFloat + x*y
  
  def m(n: Int, CALLBACK: Double => Unit): Unit =
    if (n < 0)
      CALLBACK(n.abs)
    else
      CALLBACK(PI)
}