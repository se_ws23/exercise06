package static.analysis

object Slicing {
  def poem(): Unit =
    println("Roses are red.")
    val a: Int = 1
    var b: Int = 2 // forward slicing criterion
    var c: Boolean = true
    val input: Int = scala.io.StdIn.readInt()

    if (input % 2 == 0) then c = false

    println("I like trains.")

    for i <- Range(a, 10) do
      b = b + 1
      c = !c

    println(a + b)
    println(c) // backward slicing criterion
    println("The end.")
}
