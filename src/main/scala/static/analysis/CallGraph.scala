package static.analysis

object MyNum {
  @main
  def main(): Unit =
    println("What is your birth month (number)?")
    val input = scala.io.StdIn.readInt()

    val f = fib(input)
    val r = randomize(f)

    println(s"Your magic number is $r")

  def randomize(input: Int): Int =
    val start = Zero()
    scala.util.Random.between(start.intValue, input + 1)

  def fib(a: Int): Int =
    println("processing...")
    a match
      case 0 => 0
      case 1 => 1
      case n => fib(n - 1) + fib(n - 2)
}

class Box[T](protected val data: T)
trait VeryShort extends Box[Int] {
  def intValue: Int = data
}
class Zero extends Box(0) with VeryShort
class One extends Box(1) with VeryShort
class Two extends Box(2) with VeryShort
class Three extends Box(3) with VeryShort