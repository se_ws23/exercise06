package coverage

object ConditionCoverage {
  def process(xs: List[Int], barrier: Int, default: Int): List[Int] =
    //      C1           C2
    if (xs.isEmpty | barrier <= 0)
      return Nil

    val tailAvg: Int = xs.tail.sum / xs.tail.size
    var list: List[Int] = Nil
    var i: Int = 0

    //          C3
    while (i < xs.size) {
      //       C4                 C5
      if (tailAvg == i || i * default > barrier)
        list = default :: list
      //           C6
      if ((i + 1) * barrier < 5)
        list = xs(i) * tailAvg :: list

      i = i + 1
    }
    list
}
