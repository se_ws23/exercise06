package coverage

object MCDC {
  def magic(l: Int, r: Int, diff: Int): Boolean =
    //     C1          C2          C3
    if (diff < 7 & r-l < diff & l+r <= 17)
      true
    else
      false
}
